const express = require('express');
const bodyParser = require('body-parser'); 
const mySql = require('mysql');
const app = express();
app.use(bodyParser());

const PORT = process.env.PORT || 3000;

const conn = mySql.createConnection({
    'host' : 'localhost',
    'port':3306,
    'user':"root",
    'password':"",
    'database': "hippo"
});

conn.connect(err=>{
    if(err)
    return console.log(err)
    console.log('dbconnected');
})
app.get('/users',(req,res)=>{
    conn.query('SELECT * FROM `users`',(err,users)=>{
        if(err)
        return res.send(err)
        else res.send(users)
    })
});
app.get('/users/:id',(req,res)=>{
    console.log(req.params)
    conn.query(`SELECT * FROM users where id=${req.params.id}`,(err,users)=>{
        if(err)
        return res.send(err)
        else res.send(users)
    })
})
app.post('/users/',(req,res)=>{
   const {firstName,lastName,email, password, mobile, dob} = req.body;
   //const sql = `INSERT INTO users(${null},'${firstName}', '${lastName}', '${email}', MD5('${password}'), '${dob}', ${mobile})`
    const sql = "INSERT INTO `users`(`id`, `firstName`, `lastName`, `email`, `password`, `dob`, `mobile`)" + `VALUES (${null},'${firstName}','${lastName}','${email}','${password}','2019-02-12',${mobile})`;
    conn.query(sql,(err,users)=>{
        if(err)
        return res.send(err)
        else res.send(users)
    })
})
const server = app.listen(PORT,()=>{
    console.log(` app running at ${PORT}`);
});
